import React, { Component } from 'react';
import './Navbar.css';

import { Link } from 'react-router-dom';

class Navbar extends Component {

	render() {
		return (
			<nav className="navbar">
				<span 
				className="	col-lg-12
							col-md-12
							col-sm-12
							text-white
							text-right"
				>
					<Link className="navbar-brand" to="/">logdito</Link>
				</span>
			</nav>
		);
	}
}

export default Navbar;