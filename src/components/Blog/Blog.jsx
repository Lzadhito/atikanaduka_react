import React, { Component } from 'react';
import Post from '../Post/Post';

class Blog extends Component {
	constructor(props) {
		super(props);
		this.state = {
			posts: []
		};
	}
	
	componentDidMount() {
		let url = "https://secure-shore-83633.herokuapp.com/posts"
		fetch(url)
		.then(resp => resp.json())
		.then(data => {
			let posts = data.slice(0).reverse().map((post, index) => {
				return (
					<Post
						key={index}
						title={post.title} 
						date={post.date} 
						description={post.description}
					/>
				)
			})
			this.setState({posts: posts});
		})
	}

	// createPost(posts) {
	// 	let result = [];
	// 	for (let data of posts) {
	// 		result.push(<Post title={data.post.title} meta={data.post.meta} description={data.post.description}  />)
	// 	}
	// 	return result;
	// }

	render() {
		return(
			<div className='container'>
				<div className='row'>
					<div className='col-sm-8 col-lg-12 col-md-12 blog-main'>
						{this.state.posts}
					</div>
				</div>
			</div>
		);
	}
}

export default Blog;