import React from 'react';
import photo from './header.webp';
import { Parallax } from 'react-parallax';

const HeaderImage = () => (
	<picture className="picture">
		<Parallax
            bgImage={photo}
            strength={300}
			className='img-fluid mb-5'
        >
            <div style={{ height: '100vh' }} />
        </Parallax>
 
	</picture>
);

export default HeaderImage;