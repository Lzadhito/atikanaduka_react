import React, { Component } from 'react';
import './AddPost.css';

import { Redirect } from 'react-router-dom'

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
  });

class AddPost extends Component {

	  constructor() {
	    super();

	    this.state = {
	    	title: '',
			description: '',
			isSubmitted: false,
			willRedirect: false
		}
	    this.changeState = this.changeState.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.openSuccessDialog = this.openSuccessDialog.bind(this);
		this.closeSuccessDialog = this.closeSuccessDialog.bind(this);
		this.goToHomepage = this.goToHomepage.bind(this);
	  }
	  handleSubmit(e) {
	  	e.preventDefault();
	  	this.addPost(this.state);
	  }

	  changeState = ({target: {name, value}}) => {
	  	let newState = {};
	  	newState[name] = value;
	  	this.setState(newState);
	  };

	  addPost(post) {
	  	fetch("https://secure-shore-83633.herokuapp.com/posts/", {
	  		method: "POST",
	  		headers: {
	  		    "Accept": "application/json",
	  		    "Content-Type": "application/json",
	  		},
	  		body: JSON.stringify({
				title: post.title,
				description: post.description,
			  })
		  })
		  .then(()=> {
			this.openSuccessDialog();
		  });
		  
	  }

	openSuccessDialog() {
		this.setState({ isSubmitted: true });
	}

	closeSuccessDialog() {
		this.setState({ isSubmitted: false });
	}

	goToHomepage() {
		this.setState({ isSubmitted: false, willRedirect: true });
	}

	render() {
		const {isSubmitted, willRedirect} = this.state; 
		return(
			<form className="container" onSubmit={this.handleSubmit}>
				<div className="md-form from-group pink-textarea">
				<textarea id="addpost-title" name="title" className="md-textarea form-control" placeholder="Title" rows="1" onChange={this.changeState} required></textarea>
				</div>

				<div className="md-form from-group pink-textarea">
				<textarea id="addpost-desc" name="description" className="md-textarea form-control" placeholder="add your text here...." rows="3" onChange={this.changeState} required></textarea>
				</div>
				<button type="submit" className="btn btn-primary">Submit</button>
				
				<Dialog
					open={isSubmitted}
					TransitionComponent={Transition}
					keepMounted
					onClose={this.closeSuccessDialog}
					aria-describedby="alert-dialog-slide-description"
				>
					<DialogTitle id="alert-dialog-slide-title">{"New post has been added!"}</DialogTitle>
					<DialogActions>
					<Button onClick={this.closeSuccessDialog} color="primary">
						Stay
					</Button>
					<Button onClick={this.goToHomepage} color="primary">
						Go To Homepage
					</Button>
					</DialogActions>
				</Dialog>

				{ willRedirect? <Redirect to='/' />
				: null}
			</form>
		);
	}
}

export default AddPost;