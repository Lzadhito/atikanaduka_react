import React from 'react';
import './Post.css';

const Post = ({ title, date, description }) => (
	<article className="blog-post">
		<h2 className="blog-post-title">{ title }</h2>
		<p className="blog-post-date">{ date }</p>
		<p className="blog-post-description">
			{ description }
		</p>
		<hr />
		<br />
	</article>
);

export default Post;