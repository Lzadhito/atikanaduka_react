import React, { Component } from 'react';
import Navbar from '../Navbar/Navbar';
import Main from '../Main/Main';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <Main />
        <footer className="footer-copyright text-center py-3">
        	<small>
        		logdito©2019
        	</small>
        </footer>
      </div>
    );
  }
}

export default App;
