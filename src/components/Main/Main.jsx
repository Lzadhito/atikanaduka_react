import React, { Component, Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';
import Blog from './../Blog/Blog';
import AddPost from './../AddPost/AddPost';
import './Main.css';


class Main extends Component {

	render() {
		const HeaderImage = React.lazy(() => import('./../HeaderImage/HeaderImage'));
		return(
			<main>
				<Suspense fallback=
				{<div>Loading...</div>}>
					<HeaderImage />
				</Suspense>

				<Switch>
					<Route exact path="/" component={Blog}/>
					<Route path="/thisistheurlwhereyoucanaddpost" component={AddPost}/>
				</Switch>
			</main>
		);
	}
}

export default Main;